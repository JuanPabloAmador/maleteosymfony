<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class FormularioEnviadoController extends AbstractController
{
    /**
     * @Route("/formularioenviado", name="formularioEnviado")
     */
    public function formularioEnviado()
    {
        return $this->render('Administracion/formularioEnviado.html.twig');
    }
}
