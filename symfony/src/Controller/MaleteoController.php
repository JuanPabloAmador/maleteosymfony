<?php

namespace App\Controller;

use App\Entity\Comentarios;
use App\Form\FormularioDemoType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class MaleteoController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function renderPaginaPrincipal(Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(FormularioDemoType::class);
        $form->handleRequest($request);

        $repositorioComentarios = $em->getRepository(Comentarios::class);
        $comentarios = $repositorioComentarios->findAll();

        if ($form->isSubmitted() && $form->isValid()) {

            $solicitud = $form->getData();

            $em->persist($solicitud);
            $em->flush();

            return $this->redirectToRoute('formularioEnviado');
        }

        return $this->render("Formularios/formularioDemo.html.twig", [

            'formulario' => $form->createView(),
            'comentarios' => $comentarios


        ]);
    }
}
