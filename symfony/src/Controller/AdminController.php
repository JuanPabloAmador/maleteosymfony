<?php

namespace App\Controller;

use App\Form\FormularioDemoType;
use App\Entity\Comentarios;
use App\Entity\SolicitudDemo;
use App\Form\EditarSolicitudDemoType;
use App\Repository\SolicitudDemoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends AbstractController


{

    /**
     * @Route("/admin", name="admin")
     */
    public function renderSolicitudes(EntityManagerInterface $em)
    {

        $repositorioDemo = $em->getRepository(SolicitudDemo::class);

        $solicitudes = $repositorioDemo->findAll();

        $repositorioComents = $em->getRepository(Comentarios::class);

        $comentarios = $repositorioComents->findAll();

        return $this->render("Administracion/adminDemo.html.twig", [

            'solicitudes' => $solicitudes,
            'comentarios' => $comentarios
        ]);
    }



    /**
     * @Route("/editar/solicitud/{id}", name="editar_solicitud")
     */

    public function editarSolicitud(Request $request, EntityManagerInterface $em, $id)
    {
        $solicitud = $em->getRepository(SolicitudDemo::class)->find($id);
        //dd($solicitud);

        $form = $this->createForm(EditarSolicitudDemoType::class, $solicitud);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $solicitudEditada = $form->getData();
            //dd($solicitudEditada);
            $em->persist($solicitudEditada);
            $em->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render("Administracion/adminDemoEdit.html.twig", [

            'formulario' => $form->createView(),
            'solicitudEditar' => $solicitud,


        ]);
    }
}
