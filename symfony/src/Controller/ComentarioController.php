<?php

namespace App\Controller;

use App\Entity\Comentarios;
use App\Form\EnvioComentarioType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\EditarComentarioType;

class ComentarioController extends AbstractController
{

    /**
     * @Route("/comentario")
     */
    public function renderFormularioComentario(Request $request, EntityManagerInterface $em)
    {

        $formularioComentario = $this->createForm(EnvioComentarioType::class);

        $formularioComentario->handleRequest($request);

        if ($formularioComentario->isSubmitted() && $formularioComentario->isValid()) {

            $comentario = $formularioComentario->getData();
            $em->persist($comentario);
            $em->flush();

            return $this->redirectToRoute('formularioEnviado');
        }

        return $this->render('Formularios/comentario.html.twig', [

            'formularioComentario' => $formularioComentario->createView()

        ]);
    }



    /**
     * @Route("/editar/comentario/{id}", name="editar_comentario")
     */

    public function editarComentario(Request $request, EntityManagerInterface $em, Comentarios $comentario)
    {
        // $comentario = $em->getRepository(Comentarios::class)->find($id);
        //dd($comentario);

        $formComentario = $this->createForm(EditarComentarioType::class, $comentario);
        $formComentario->handleRequest($request);

        if ($formComentario->isSubmitted() && $formComentario->isValid()) {

            $comentarioEditado = $formComentario->getData();
            //dd($comentarioEditado);
            $em->persist($comentarioEditado);
            $em->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render("Administracion/adminComentEdit.html.twig", [

            'formulario' => $formComentario->createView(),


        ]);
    }
}
