<?php

namespace App\Form;

use App\Entity\SolicitudDemo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormularioDemoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class, [
                'attr' => [
                    'placeholder' => 'Lonnie Holt'
                ]
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'lonnie.holt@example.com'
                ]

            ])
            ->add('horarioPreferido', ChoiceType::class, [
                'choices'  => [
                    'Mañana' => 'mañana',
                    'Tarde' => 'tarde',
                    'Noche' => 'noche',
                ],
            ])
            ->add('ciudad', ChoiceType::class, [
                'choices' => [
                    'Madrid' => 'Madrid',
                    'Barcelona' => 'Barcelona',
                    'Sevilla' => 'Sevilla',
                    'Bilbao' => 'Bilbao',
                    'Oviedo' => 'Oviedo',
                    'Salamanca' => 'Salamanca'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SolicitudDemo::class,
        ]);
    }
}
