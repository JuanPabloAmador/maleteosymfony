<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Comentarios;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditarComentarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('nombre', null, [
                'attr' => [
                    'placeholder' => 'nombre',

                ]
            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'correo@electronico.com'
                ]
            ])
            ->add('lugar', ChoiceType::class, [
                'attr' => [
                    'placeholder' => 'Ciudad'
                ],
                'choices' => [
                    'Madrid' => 'Madrid',
                    'Barcelona' => 'Barcelona',
                    'Sevilla' => 'Sevilla',
                    'Bilbao' => 'Bilbao',
                    'Oviedo' => 'Oviedo',
                    'Salamanca' => 'Salamanca'
                ]

            ])
            ->add('comentario', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Añade tu comentario'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comentarios::class,
        ]);
    }
}
