<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    public function index()
    {
        return $this->render('base.html.twig');
        
    }


    /**
     * @Route("/saludar/{nombre}")
     */
    public function saludo($nombre)
    {
        return new Response("Hola $nombre");
    }


    /**
     * @Route("/saludar/{nombre}/{tipo}")
     */
    public function saludarComplicado($nombre, $tipo)
    {
        $saludo ='';
        if ($tipo == "mayusculas") {

            $saludo = strtoupper("hola $nombre");

            return new Response($saludo);
        }
        else {
            return new Response("Hola $nombre");
        }
    }





    // public function contacto(){
    //     die("Estoy en contacto");
    // }

}
