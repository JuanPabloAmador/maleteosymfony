# maleteoSymfony

Ejercicio de creación de aplicación Web con **PHP** y el framework **Symfony**

La aplicación conecta a **mysql** en local, recoge la información del formulario y la persiste en base de datos. 
Por otro lado se crearon rutas de administración en las que se muestra la información almacenada en base de datos, 
y se implmemento el CRUD básico pudiendo editar y eliminar dicha información, 
para pintar todas las vistas se utiliza el motor de render **Twig**.
